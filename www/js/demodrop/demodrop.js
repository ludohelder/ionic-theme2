angular.module('your_app_name.api.services', [])

.service('DemoDrop', function($http, AuthService) {

  var endpoint = 'https://api.demodrop.com';
  var apiKey = 'zOEHBxgebk6ti3iTh9MmaFrv5ZPEUla4zDqY9Lvb';

  var headers = {
    'x-api-key': apiKey,
  };

  var DemoDrop = {
    get: function(path, params){
      return this.call('GET', path, params);
    },
    post: function(path, data){
      return this.call('POST', path, data);
    },
    delete: function(path, data){
      return this.call('DELETE', path, data);
    },
    patch: function(path, data){
      return this.call('PATCH', path, data);
    },
    call: function(method, path, variables){
      var user = AuthService.getLoggedUser();
      if (user && user.token){
        headers.Authorization = 'Bearer ' + user.token;
      }
      if (method == 'GET'){
        var data = variables;
        var params = null;
      }
      else{
        var params = variables;
        var data = null;
      }

      return $http({
        method: method,
        url: endpoint + path,
        data: data,
        headers: headers
      });
    },
    errorHandler: function(result){
     // if (result.status === 401){
        console.log('call error');
        console.log(result);
      // }
    },
  }

  return DemoDrop;
})
