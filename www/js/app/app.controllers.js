angular.module('your_app_name.app.controllers', [])


.controller('AppCtrl', function($scope, AuthService) {

  $scope.loggedUser = AuthService.getLoggedUser();

})


.controller('ProfileCtrl', function($scope, $stateParams, DemoDrop, $ionicHistory, $state, $ionicScrollDelegate) {

  $scope.$on('$ionicView.afterEnter', function() {
    $ionicScrollDelegate.$getByHandle('profile-scroll').resize();
  });

  var userId = $stateParams.userId;

  $scope.myProfile = $scope.loggedUser.id == userId;
  $scope.tracks = [];
  $scope.likes = [];
  $scope.user = {};

  DemoDrop.get('/users/' + userId).then(function(result){
    $scope.user = result.data;
  });

  DemoDrop.get('/users/' + userId + '/tracks').then(function(result){
    $scope.tracks = result.data.data;
  });

  // Todo: likes
  // DemoDrop.get('/users/' + userId + '/tracks').then(function(result){
  //   $scope.likes = result.data.data;
  // });

  $scope.getUserLikes = function(userId){
    //we need to do this in order to prevent the back to change
    $ionicHistory.currentView($ionicHistory.backView());
    $ionicHistory.nextViewOptions({ disableAnimate: true });

    $state.go('app.profile.likes', {userId: userId});
  };

  $scope.getUserPosts = function(userId){
    //we need to do this in order to prevent the back to change
    $ionicHistory.currentView($ionicHistory.backView());
    $ionicHistory.nextViewOptions({ disableAnimate: true });

    $state.go('app.profile.posts', {userId: userId});
  };


  $scope.followUser = function(user){
    DemoDrop.post('/users/' + user.id + '/followers', function(result){
    user.is = {};
    user.is.following = true;
    }, DemoDrop.errorHandler);
  }

  $scope.unfollowUser = function(user){
    DemoDrop.post('/users/' + user.id + '/followers', function(result){
      user.is = {};
      user.is.following = false;
    }, DemoDrop.errorHandler);
  }


})


.controller('TrackCtrl', function($scope, $stateParams, $ionicLoading, DemoDrop) {
  var trackId = $stateParams.trackId;

  DemoDrop.get('/tracks/' + trackId).then(function(result){
    $scope.track = result.data;
  }, DemoDrop.errorHandler);

})


.controller('PostCardCtrl', function($scope, PostService, $ionicPopup, $state, DemoDrop, AudioSvc) {
  var commentsPopup = {};

  $scope.navigateToUserProfile = function(user){
    commentsPopup.close();
    $state.go('app.profile.posts', {userId: user.id});
  };

  $scope.pauseTrack = function(track){
    track.is = {
      playing: false
    };
    AudioSvc.pauseAudio();
  }

  $scope.playTrack = function(track){
    console.log('play' + track.id);

    track.is = {
      playing: true
    };

    $scope.playingTrack = track.id;

    DemoDrop.post('/tracks/' + track.id + '/plays').then(function(result){

      AudioSvc.playAudio(result.data.audio, function(a, b) {
        console.log(a, b);
        $scope.position = Math.ceil(a / b * 100);
        if (a < 0) {
          AudioSvc.stopAudio();
        }
        // $scope.$apply();
      });

    }, DemoDrop.errorHandler);
  }

  $scope.likeTrack = function(track){

    DemoDrop.post('/tracks/' + track.id + '/likes').then(function(){
      track.is.liked = true;
    });


  }

  $scope.unlikeTrack = function(track){

    DemoDrop.delete('/tracks/' + track.id + '/likes').then(function(){
      track.is.liked = false;
    });
  }

  $scope.showComments = function(track) {
    DemoDrop.get('/tracks/' + track.id + '/comments').then(function(result){

    }, DemoDrop.errorHandler);
    // PostService.getPostComments(post)
    // .then(function(data){
    //   post.comments_list = data;
    //   commentsPopup = $ionicPopup.show({
  		// 	cssClass: 'popup-outer comments-view',
  		// 	templateUrl: 'views/app/tracks/comments.html',
  		// 	scope: angular.extend($scope, {current_post: post}),
  		// 	title: 'Comments',
  		// 	buttons: [
  		// 		{ text: '', type: 'close-popup ion-ios-close-outline' }
  		// 	]
  		// });
    // });
	};
})

.controller('FeedCtrl', function($scope, $ionicPopup, $state, DemoDrop) {
  $scope.tracks = [];
  $scope.page = 0;
  $scope.totalPages = 9999;

  $scope.doRefresh = function() {
    console.log('doRefresh');
    DemoDrop.get('/tracks', {sort: 'trending'}).then(function(result){
      //$scope.totalPages = 99999;
      $scope.tracks = result.data.data;

      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.getNewData = function() {
    //do something to load your new data here
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    DemoDrop.get('/tracks', {sort: 'trending', page: $scope.page}).then(function(result){
      //$scope.totalPages = data.totalPages;
      // console.log(result.data);
      $scope.tracks = $scope.tracks.concat(result.data.data);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };

  // $scope.doRefresh();

})


.controller('ShopCtrl', function($scope, DemoDrop) {
  $scope.tracks = [];
  $scope.popular_products = [];

  DemoDrop.get('/tracks').then(function(result){
    $scope.tracks = result.data.data;
  });

  // ShopService.getProducts().then(function(products){
  //   $scope.popular_products = products.slice(0, 2);
  // });
})


.controller('CheckoutCtrl', function($scope) {
  //$scope.paymentDetails;
})

.controller('SettingsCtrl', function($scope, $ionicModal) {

  $ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.terms_of_service_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.privacy_policy_modal = modal;
  });

  $scope.showTerms = function() {
    $scope.terms_of_service_modal.show();
  };

  $scope.showPrivacyPolicy = function() {
    $scope.privacy_policy_modal.show();
  };

})

// DemoDrop
.controller('BrowseCtrl', function($scope, DemoDrop) {
  $scope.tracks = [];
  $scope.page = 0;
  $scope.totalPages = 9999;

  $scope.doRefresh = function() {
    DemoDrop.get('/tracks', {sort: 'trending'}).then(function(result){
      //$scope.totalPages = 99999;
      $scope.tracks = result.data.data;

      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.getNewData = function() {
    //do something to load your new data here
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    DemoDrop.get('/tracks', {sort: 'trending', page: $scope.page}).then(function(result){
      //$scope.totalPages = data.totalPages;
      $scope.tracks = $scope.tracks.concat(result.data.data);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };

})


.controller('CardsCtrl', function($scope, TDCardDelegate, $timeout, DemoDrop) {

  var cardTypes = [
    { image: 'http://c4.staticflickr.com/4/3924/18886530069_840bc7d2a5_n.jpg' }
  ];


  $scope.cards = {
    master: Array.prototype.slice.call(cardTypes, 0),
    active: Array.prototype.slice.call(cardTypes, 0),
    discards: [],
    liked: [],
    disliked: []
  }

  $scope.cardDestroyed = function(index) {
    // stop current track
    console.log('stop track', $scope.cards.active[0]);
    $scope.cards.active.splice(index, 1);
    console.log('play track', $scope.cards.active[0]);
    // play next track
  };

  $scope.addCard = function() {
    var newCard = cardTypes[0];
    $scope.cards.active.push(angular.extend({}, newCard));
  }

  $scope.refreshCards = function() {
    // Set $scope.cards to null so that directive reloads
    $scope.cards.active = null;
    DemoDrop.get('/tracks').then(function(result){
      $scope.cards.active = result.data.data;
    });

  }

  $scope.$on('removeCard', function(event, element, card) {
    var discarded = $scope.cards.master.splice($scope.cards.master.indexOf(card), 1);
    $scope.cards.discards.push(discarded);
  });

  $scope.cardSwipedLeft = function(index) {
    var card = $scope.cards.active[index];
    $scope.cards.disliked.push(card);

    // DemoDrop.post('/tracks/'+index+'/dislikes');

  };
  $scope.cardSwipedRight = function(index) {
    var card = $scope.cards.active[index];
    $scope.cards.liked.push(card);

    // DemoDrop.post('/tracks/'+index+'/likes');
  };

})

.controller('CardCtrl', function($scope, TDCardDelegate) {
  console.log('CardCtrl');
})


;
