angular.module('your_app_name.app.services', [])

.service('AuthService', function (){

  this.saveUser = function(user){
    window.localStorage.demodrop_user = JSON.stringify(user);
  };

  this.getLoggedUser = function(){

    return (window.localStorage.demodrop_user) ?
      JSON.parse(window.localStorage.demodrop_user) : null;
  };

})

.service('PostService', function ($http, $q, DemoDrop){

  this.getPostComments = function(post){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      var comments_users = database.users;

      // Randomize comments users array
      comments_users = window.knuthShuffle(comments_users.slice(0, post.comments));

      var comments_list = [];
      // Append comment text to comments list
      comments_list = _.map(comments_users, function(user){
        var comment = {
          user: user,
          text: database.comments[Math.floor(Math.random()*database.comments.length)].comment
        };
        return comment;
      });

      dfd.resolve(comments_list);
    });

    return dfd.promise;
  };

  this.getUserDetails = function(userId){
    var dfd = $q.defer();

    DemoDrop.get('/users/' + userId).then(function(result) {
      dfd.resolve(result.data);
    }, DemoDrop.errorHandler);

    return dfd.promise;
  };

  this.getUserPosts = function(userId){
    var dfd = $q.defer();

    DemoDrop.get('/tracks').then(function(results) {

      dfd.resolve(results.data);

    }, DemoDrop.errorHandler);

    return dfd.promise;
  };

  this.getUserLikes = function(userId){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      //get user likes
      //we will get all the posts
      var slicedLikes = database.posts.slice(0, 4);
      // var sortedLikes =  _.sortBy(database.posts, function(post){ return new Date(post.date); });
      var sortedLikes =  _.sortBy(slicedLikes, function(post){ return new Date(post.date); });

      //add user data to posts
      var likes = _.each(sortedLikes.reverse(), function(post){
        post.user = _.find(database.users, function(user){ return user._id == post.userId; });
        return post;
      });

      dfd.resolve(likes);

    });

    return dfd.promise;

  };

  this.getFeed = function(page){

    var pageSize = 5, // set your page size, which is number of records per page
        skip = pageSize * (page-1),
        totalPosts = 1,
        totalPages = 1,
        dfd = $q.defer();

    DemoDrop.get('/tracks', {sort: 'trending'}).then(function(result) {

      dfd.resolve({
        tracks: result.data.data,
        totalPages: 10
      });

    }, DemoDrop.errorHandler);

    return dfd.promise;
  };
})


;
