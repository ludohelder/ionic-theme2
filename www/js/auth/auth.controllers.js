angular.module('your_app_name.auth.controllers', [])

.controller('AuthCtrl', function($scope){

})

.controller('WelcomeCtrl', function($scope, $ionicModal, show_hidden_actions, $state){

	$scope.show_hidden_actions = show_hidden_actions;

	$scope.toggleHiddenActions = function(){
		$scope.show_hidden_actions = !$scope.show_hidden_actions;
	};

	$scope.facebookSignIn = function(){
		console.log("doing facebbok sign in");
		$state.go('app.feed');
	};

	$scope.googleSignIn = function(){
		console.log("doing google sign in");
		$state.go('app.feed');
	};

	$scope.twitterSignIn = function(){
		console.log("doing twitter sign in");
		$state.go('app.feed');
	};

	$ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.privacy_policy_modal = modal;
  });

	$ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.terms_of_service_modal = modal;
  });

  $scope.showPrivacyPolicy = function() {
    $scope.privacy_policy_modal.show();
  };

	$scope.showTerms = function() {
    $scope.terms_of_service_modal.show();
  };
})

.controller('LogInCtrl', function($scope, $state, DemoDrop, AuthService){
	$scope.doLogIn = function(user){

    $scope.error = '';

    DemoDrop.post('/users/login', {
      username: user.email,
      password: user.password
    }).then(function(result){
      AuthService.saveUser(result.data);
      $state.go('app.feed');
    }, function(result){
      $scope.error = result.data.message;
    });

	};
})

.controller('SignUpCtrl', function($scope, $state, DemoDrop){
	$scope.doSignUp = function(user){
		console.log("doing sign up");

    $scope.error = '';

    DemoDrop.post('/users', user).then(function(){
      $state.go('app.feed');
    }, function(result){
      $scope.error = result.data.message;
    });

	};
})

.controller('ForgotPasswordCtrl', function($scope, $state, DemoDrop){
	$scope.requestNewPassword = function(user) {
    console.log("requesting new password");

    DemoDrop.delete('/users/password', { user: user.email })
    .then(function(){
      $state.go('auth.login', {passwordReset: true});
    }, function(result){
      $scope.error = result.data.message;
    });

  };
})

;
